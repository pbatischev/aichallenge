# Зависимости

* Ruby > 2.0
* Rails > 4.0

# Запуск сервера

## Production

1. bundle install
1. rake assets:precompile
2. rake db:migrate RAILS_ENV="production"
3. rails server -e production

## Development

1. bundle install
1. rake db:migrate
1. rails server
